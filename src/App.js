import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>Successfully Deployed <code>A React application</code> to Netlify with CircleCI</p>
        <p>some changes</p>
        <p>more changes</p>
        <p>project version now exists: {process.env.REACT_APP_VERSION || 'damn'}</p>
        <p>now i've added git bash tags 😎</p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
